# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# app
PRODUCT_PACKAGES += \
    arcore \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    CarrierMetrics \
    ConnectivityMonitor \
    FaceLock \
    GoogleCamera \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GoogleVrCore \
    LatinIMEGooglePrebuilt \
    MarkupGoogle \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    talkback \
    Tycho \
    WallpapersBReel18

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GCS \
    GoogleBackupTransport \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleRestore \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    NexusLauncherPrebuilt \
    Phonesky \
    PrebuiltGmsCorePi \
    SettingsIntelligenceGooglePrebuilt \
    RetroMusicPlayer \
    SetupWizard \
    StorageManagerGoogle \
    TagGoogle \
    TurboPrebuilt \
    Velvet \
    WallpaperPickerGooglePrebuilt \
    WellbeingPrebuilt

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/pixelgapps/overlay/

# Props
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.ime.theme_id=5 \
    ro.wallpapers_loc_request_suw=true \
    ro.opa.eligible_device=true \
    setupwizard.theme=glif_v3_light \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    ro.setupwizard.enterprise_mode=1 \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent \
    ro.setupwizard.rotation_locked=true \
    ro.wallpapers_loc_request_suw=true

# Libraries
PRODUCT_COPY_FILES += \
    vendor/pixelgapps/lib64/libbarhopper.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libbarhopper.so \
    vendor/pixelgapps/lib64/libgdx.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libgdx.so \
    vendor/pixelgapps/lib64/libwallpapers-breel-jni.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libwallpapers-breel-jni.so \
    vendor/pixelgapps/lib64/libsketchology_native.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libsketchology_native.so

# Symlinks
PRODUCT_PACKAGES += \
    libbarhopper.so \
    libfacenet.so \
    libgdx.so \
    libwallpapers-breel-jni.so \
    libjpeg.so \
    libsketchology_native.so

ifneq ($(filter marlin sailfish, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016

PRODUCT_COPY_FILES += \
    vendor/pixelgapps/etc/permissions/com.google.android.camera.experimental2016.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2016.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_2016_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2016_exclusive.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2017.xml
endif
ifneq ($(filter taimen walleye, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2017

PRODUCT_COPY_FILES += \
    vendor/pixelgapps/etc/permissions/com.google.android.camera.experimental2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2017.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_2017_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017_exclusive.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2017.xml
endif

# Blobs
PRODUCT_COPY_FILES += \
    vendor/pixelgapps/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default-permissions/default-permissions.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.maps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.maps.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.media.effects.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.media.effects.xml \
    vendor/pixelgapps/etc/permissions/com.google.widevine.software.drm.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/pixelgapps/etc/permissions/privapp-permissions-googleapps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-googleapps.xml \
    vendor/pixelgapps/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/preferred-apps/google.xml \
    vendor/pixelgapps/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google.xml \
    vendor/pixelgapps/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_build.xml \
    vendor/pixelgapps/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/pixelgapps/etc/sysconfig/google_vr_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_vr_build.xml \
    vendor/pixelgapps/lib/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfilterpack_facedetect.so \
    vendor/pixelgapps/lib/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfrsdk.so \
    vendor/pixelgapps/lib64/libfacenet.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfacenet.so \
    vendor/pixelgapps/lib64/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfrsdk.so \
    vendor/pixelgapps/lib64/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfilterpack_facedetect.so

